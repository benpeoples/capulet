# Capulet Receiver

This is a fairly straightforward Arduino program (written originally in about 2014, so unsure if it's compatible with current IDE).

Uses Xbee library to parse API packets.

The Capulet hardware has 6 DIP switches and 6 FETs on the `AnalogWrite` pins on an atmega328p.   

### Basic program loop:

1. Wait for Xbee Packet
2. Update `set` array
3. Write contents of `set` array to the FETs

Steps 2 and 3 are separate because it allows us to code custom functionality (one user wanted the outputs to be candle flickers), and splitting this way makes code reuse easier.  

### Xbee configuration

Set the PAN ID to the same as the transmitter, and baud rate to 115200 otherwise default settings.
