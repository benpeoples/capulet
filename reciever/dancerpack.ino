// Copyright (c) 2017 Ben Peoples Industries LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#include <avr/wdt.h>
#include <XBee.h>

uint8_t UNIVERSE=1; // This hard codes which page to be reading (1 and 2 are by default sent)

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif


XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
// create reusable response objects for responses we expect to handle
Rx16Response rx16 = Rx16Response();
Rx64Response rx64 = Rx64Response();

uint8_t option = 0;
uint8_t set[6];


//uint8_t sw1, sw2, sw3, sw4;
uint8_t address;
uint8_t offset;

uint8_t i;
uint8_t step;
uint32_t timer;

void setup()
{
  wdt_disable();
  wdt_reset();
  timer = millis() + 250;

 xbee.begin(115200);

  pinMode(A3,INPUT); // SW1
  digitalWrite(A3,HIGH);
  pinMode(A2,INPUT); // SW2
  digitalWrite(A2,HIGH);
  pinMode(A1,INPUT); // SW3
  digitalWrite(A1,HIGH);
  pinMode(A0,INPUT); // SW4
  digitalWrite(A0,HIGH);

    pinMode(A4,INPUT); // SW5
  digitalWrite(A4,HIGH);
    pinMode(A5,INPUT); // SW6
  digitalWrite(A5,HIGH);

  if(digitalRead(A3) == LOW)
  {
      address += 1;
  }
  if(digitalRead(A2) == LOW)
  {
      address += 2;
  }
  if(digitalRead(A1) == LOW)
  {
      address += 4;
  }
  if(digitalRead(A0) == LOW)
  {
      address += 8;
  }

  if(digitalRead(A4) == LOW) // If Switch 4 is on (LOW), then we're reading the second page
 {
	 UNIVERSE = 2;
  }

  offset = address*6 + 4;   // Each pack takes a slice of 6 slots, starting from offset 4 (after our 4 byte header)

  wdt_reset();
  wdt_enable(WDTO_30MS); // These have a 30ms WDT
  wdt_reset();
}


void loop()
{
  wdt_reset();
  xbee.readPacket();

  wdt_reset();
  if (xbee.getResponse().isAvailable()) {
    // got something

    if (xbee.getResponse().getApiId() == RX_16_RESPONSE || xbee.getResponse().getApiId() == RX_64_RESPONSE) {
      // got a rx packet

      if (xbee.getResponse().getApiId() == RX_16_RESPONSE) {
        xbee.getResponse().getRx16Response(rx16);
        option = rx16.getOption();
        if(rx16.getData(0) == 0x55) // Data packet
        {
          if(rx16.getData(1) == UNIVERSE) // Our Universe
          {
            set[0] = rx16.getData(offset);
            set[1] = rx16.getData((offset+1));
            set[2] = rx16.getData((offset+2));
            set[3] = rx16.getData((offset+3));
            set[4] = rx16.getData((offset+4));
            set[5] = rx16.getData((offset+5));
              wdt_reset();
  analogWrite(3,set[0]);
  analogWrite(5,set[1]);
  analogWrite(6,set[2]);
  analogWrite(9,set[3]);
  analogWrite(10,set[4]);
  analogWrite(11,set[5]);

          }
        }
      }
      else {
        xbee.getResponse().getRx64Response(rx64);
        option = rx64.getOption();
        if(rx64.getData(0) == 0x55) // Data packet
        {
          if(rx64.getData(1) == UNIVERSE) // Our Universe
          {
            set[0] = rx64.getData(offset);
            set[1] = rx64.getData((offset+1));
            set[2] = rx64.getData((offset+2));
            set[3] = rx64.getData((offset+3));
            set[4] = rx64.getData((offset+4));
            set[5] = rx64.getData((offset+5));
              wdt_reset();
  analogWrite(3,set[0]);
  analogWrite(5,set[1]);
  analogWrite(6,set[2]);
  analogWrite(9,set[3]);
  analogWrite(10,set[4]);
  analogWrite(11,set[5]);

          }
        }
      }

  wdt_reset();


    }
    else if (xbee.getResponse().isError()) {

     // analogWrite(11,255);

    }

  }

    wdt_reset();

}
