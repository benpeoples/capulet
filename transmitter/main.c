// Copyright (c) 2017 Ben Peoples Industries LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


// This uses an 8MHz clock, adjust here but also in UART calculation
#define F_CPU 8000000UL

#include <avr/io.h>
#include <inttypes.h>
#include "bitfield.h"
#include <avr/wdt.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <math.h>

#define SET_DDRB 0b00100111
#define SET_DDRC 0b00111101
#define SET_DDRD 0b00010000

// Initial PORT settings (pullups)

#define SET_PORTB 0b10000001
#define SET_PORTC 0b00000010
#define SET_PORTD 0b11100000

// DMX Data
volatile uint8_t waiting_for_break = 1;
volatile uint8_t waiting_for_frametype;
volatile uint16_t slot;
volatile uint8_t frametype;
volatile uint16_t start_address = 0;
volatile uint8_t checksum;
volatile uint8_t i;

volatile uint8_t payload0[100];
volatile uint8_t payload1[100];

volatile uint8_t checksum;

volatile uint8_t mode = 0; // 0 = 16 , 1 = 32

volatile uint16_t step = 108;

// This line because this code was ported from the atmega644p which has two UARTs
#define UDR1 UDR0

int main(void)
{
	wdt_disable(); // Disable WDT on startup

	cli();

	// Data direction registers
	DDRB = SET_DDRB;
	DDRC = SET_DDRC;
	DDRD = SET_DDRD;


	// Ports
	PORTB = SET_PORTB;
	PORTC = SET_PORTC;
	PORTD = SET_PORTD;

		// Input
	UBRR0L = 1; // 250k at 8MHz
	UBRR0H = 0;
	UCSR0C = 0b00000110;
	UCSR0B = 0b10011000; // TXRX with interrupts
	UCSR0A = 0b00000000;

	UDR1 = 0x55;

	sei();

	wdt_reset();
	start_address = 1; // Set Start Address Here
	switch_settings = 0;

    payload0[0] = 0x55;  // This is the start of DMX data magic number
	  payload0[1] = 0x01;  // This field indicates the page of DMX data (96 slots per page)
	  payload0[2] = start_address; // this is the start address for the first page
	  payload0[3] = start_address >> 8;

	  payload1[0] = 0x55;  // This is the start of DMX data magic number
	  payload1[1] = 0x02;
		payload1[2] = start_address;
		payload1[3] = start_address >> 8;

	wdt_reset();
	wdt_enable(WDTO_120MS); // Enables 120ms watchdog reset
	wdt_reset();

	wdt_reset();

	while(1)
	{
	wdt_reset();
	if(UCSR0A & 0b00100000) // Tranmsit buffer empty
	{
		step++; // Step is a state machine tracker

		if(step >= 500 && mode == 0)  // if Mode = 0, only transmit one page, otherwise transmit two
		{
			step = 0;
			checksum = 0;
			UDR1 = 0x7E; // Write Xbee header
	    }
		else if(step >= 1000)
		{
			step = 0;
			checksum = 0;
			UDR1 = 0x7E;
		}
		else
		{
			if(step == 1)
			{
				UDR1 = 0x00;  // More Xbee header
			}
			else if(step == 2)
			{
				UDR1 = 105;
			}
			else if(step == 3)
			{
				UDR1 = 0x01;
			}
			else if(step == 4)
			{
				UDR1 = 0x00;
			}
			else if(step == 5)
			{
				UDR1 = 0xFF;
			}
			else if(step == 6)
			{
				UDR1 = 0xFF;
			}
			else if(step == 7)
			{
				UDR1 = 0x01;
			}
			else if(step >= 8 && step < 108)
			{
				UDR1 = payload0[step - 8];  // now we transmit the payload
				checksum += payload0[step - 8]; // and calculate the checksum
			}
			else if(step == 108)
			{
				UDR1 = 0xFF - checksum; // final checksum calcuation
			}
			else if(step == 500)
			{
				UDR1 = 0x7E;
				checksum = 0;
			}
			if(step == 501)
			{
				UDR1 = 0x00;
			}
			else if(step == 502)
			{
				UDR1 = 105;
			}
			else if(step == 503)
			{
				UDR1 = 0x01;
			}
			else if(step == 504)
			{
				UDR1 = 0x00;
			}
			else if(step == 505)
			{
				UDR1 = 0xFF;
			}
			else if(step == 506)
			{
				UDR1 = 0xFF;
			}
			else if(step == 507)
			{
				UDR1 = 0x01;
			}
			else if(step >= 508 && step < 608)
			{
				UDR1 = payload1[step - 508];
				checksum += payload1[step - 508];
			}
			else if(step == 608)
			{
				UDR1 = 0xFF - checksum;
			}
		}
	}
}

return 0;
}


ISR(USART_RX_vect) // Interrupt driven receipt of DMX
{
	uint8_t a = UCSR0A;
	uint8_t c = UDR0;



	if(a & 0b00010000) // Framing error means break;
	{
		waiting_for_break = 0;
		waiting_for_frametype = 1;

	}
	else if(waiting_for_break == 0 && waiting_for_frametype == 1) // state tracking
	{
		frametype = c; // Read another byte
		waiting_for_frametype = 0;
		if(frametype == 0x00) // yes, we check frametype
		{
			slot = 0;
		}

	}
	else if(waiting_for_break == 0)
	{
		if(frametype == 0x00)
		{

		slot++;
				// Splits the received packets into payload0 and payload1 accordingly
				if(slot >= start_address && slot < start_address + 96)
				{
					payload0[slot-start_address+4] = c;
				}
				else if(slot >= start_address + 96 && slot < start_address + 192)
				{
					payload1[slot-start_address-92] = c;
				}
				else if(slot == 0) // Missed something
				{
					slot = 512;
				}
		}

	}


}
