# Capulet Transmitter

This is written in straight avr-gcc for the atmega328p.  I stripped out the DIP switch reading code, so start and mode need to be set manually.

Yes, we read DMX from the same UART as we're transmitting the Xbee data.

### Basic program function:

1. Configure ports and UART settings
2. Main loop spins output to the Xbee, manually assembling API broadcast packets.  Timing is controlled by the UART's buffer, and an additional counter (step from ~108 to 500 is idling), which gets us our ~10k slot per second (or about 100 API packets per second)
3. RX interrupt handles DMX reception.  As with most ATMEGA-based recievers, we treat Framing Errors as BREAKS.   This has worked well in production, but technically doesn't follow the ANSI standard.


### Xbee Configuration

Standard S1 Xbee modules.  We use a PRO on the base station.   You have to manually (using AT commands) set the baud rate to 250000.   Set destination address to 0, etc.  
