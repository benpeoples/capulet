# Trinculo's Attic Capulet System

This is the source for the TA Capulet system, which we stopped manufacturing about a year ago.   Originally developed in 2012/2013 to support EL Wire Costumes.  

It, notably, implements 192 channels of DMX over Xbee at 50Hz in a trivial protocol.  

The system consists of two parts:

## Capulet Base Station

Base station is an atmega328p with an 8MHz clock.    DMX RX via rs485 interface ic to hardware UART, Xbee TX via (same) hardware UART.

## Capulet Remote

Another atmega328p with an 8MHZ clock, puts a FET on each of the `AnalogWrite` supported pins.   Xbee RX through the hardware UART.  


## License

Released under the MIT license:

Copyright (c) 2017 Ben Peoples Industries LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
